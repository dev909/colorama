package net.dev909.colorama.data;

import net.dev909.colorama.data.commands.CommandPaintbook;
import net.dev909.colorama.proxy.CommonProxy;
import net.minecraft.init.Blocks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = ColoramaMod.MODID, name = ColoramaMod.MODNAME, version = ColoramaMod.VERSION, updateJSON = ColoramaMod.UPDATE_JSON)
public class ColoramaMod 
{
	
    public static final String MODID = "colorama";
    public static final String MODNAME = "Colorama";
    public static final String CLIENT_PROXY = "net.dev909.colorama.proxy.ClientProxy";
    public static final String COMMON_PROXY = "net.dev909.colorama.proxy.CommonProxy";
    public static final String VERSION = "beta_0.1";
    public static final String UPDATE_JSON = "https://bitbucket.org/dev909/colorama/src/8b1280e7af08272c03b65cd93cb20fcae5ea9a1e/update.json?at=master";
    
	@SidedProxy(clientSide = CLIENT_PROXY, serverSide = COMMON_PROXY)
	public static CommonProxy proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	ColoramaReg.init();
    	//Config
    	ColoramaReg.initItems();
    	ColoramaReg.initBlocks();
    	ColoramaReg.initTileEntities();
    	ColoramaReg.initOreDict();
    	
    	proxy.preInit();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	ColoramaReg.registerRecipes();
    	//Recipes
		// some example code
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	MinecraftForge.EVENT_BUS.register(new ColoramaEventHandler());
    }
    
    @EventHandler
    public void serverLoad(FMLServerStartingEvent event) {
    	
    event.registerServerCommand(new CommandPaintbook());
    }
}
