package net.dev909.colorama.data.utils;

import net.minecraft.item.ItemStack;

public interface IMetablock {

	public String getNameByMeta(int meta);
}
