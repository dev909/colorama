package net.dev909.colorama.data.utils;

import scala.actors.threadpool.Arrays;

public class cUtils {
	public static int convertToInt(int... array) {
		try {
			return (array[0] << 16) | (array[1] << 8) | array[2];
		} catch(Exception e) {
			return 0;
		}
	}
	
	public static void Clone(int[] intoThis, int[] copyThis) {
		System.arraycopy(copyThis, 0, intoThis, 0, copyThis.length);
	}
	public static int[] Clone(int[] src) {
        int[] array = new int[src.length];
        Clone(array, src);
        return array;
	}

	public static void mergeColors(cColor color, int[] COLOR) {
		int[] avg = new int[3];
		int avgMax, maxOfAvg;
		double gain;
		color.totalMax = color.totalMax + MaximalValue(COLOR);
		color.num++;
		for (int i = 0; i < color.total.length; i++) {
			avg[i] = (color.total[i] = color.total[i] + COLOR[i]) / color.num;
		}

		avgMax = color.totalMax / color.num;
		maxOfAvg = MaximalValue(avg);
		if (maxOfAvg == 0) {
			gain = 1.0;
		} else {
			gain = (double) (avgMax / maxOfAvg);
		}

		System.out.println("> avgMAX: " + avgMax + "/nmaxOfAvg: " + maxOfAvg + "GAIN: " + gain);

		for (int i = 0; i < color.rgb.length; i++) {
			color.rgb[i] = (int) (avg[i] * gain);
		}
		System.out.println("> MERGECOLORS: " + color.rgb[0] + " " + color.rgb[1] + " " + color.rgb[2]);
	}

	public static int MaximalValue(int[] arr) {
		int j = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > j) {
				j = arr[i];
			}
		}
		return j;
	}
}
