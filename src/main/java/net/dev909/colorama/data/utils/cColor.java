package net.dev909.colorama.data.utils;

import java.util.Arrays;

public class cColor {

	public int[] rgb, total;
	public int totalMax, num;

	public cColor(int[] rgb, int[] total, int totalMax, int num) {
        this.rgb = cUtils.Clone(rgb);
		this.total = cUtils.Clone(total);
		this.totalMax = totalMax;
		this.num = num;
	}

	public cColor() {
	}

}
