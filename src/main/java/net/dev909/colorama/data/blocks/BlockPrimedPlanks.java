package net.dev909.colorama.data.blocks;

import net.dev909.colorama.data.ColoramaReg;
import net.minecraft.block.material.Material;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;

public class BlockPrimedPlanks extends CBlock {

	public BlockPrimedPlanks(Material materialIn, String name) {
		super(materialIn, name);
		this.setHardness(2.0F);
		this.setResistance(5.0F);
		this.setStepSound(soundTypeWood);
		this.setCreativeTab(ColoramaReg.cTab);
	}
	
	@Override
	public boolean isFlammable(IBlockAccess world, BlockPos pos, EnumFacing face) {
		return true;
	}

}
