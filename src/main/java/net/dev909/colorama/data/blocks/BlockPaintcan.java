	package net.dev909.colorama.data.blocks;

import java.util.List;

import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.utils.cUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import scala.actors.threadpool.Arrays;

public class BlockPaintcan extends Block {

	public final int[] COLOR;
	public static final PropertyInteger VOLUME = PropertyInteger.create("volume", 0, 4);

	public BlockPaintcan(Material material, MapColor mapColor, String name, int[] color) {
		super(material, mapColor);
		this.setUnlocalizedName(name);
		this.setStepSound(soundTypeMetal);
		this.setHardness(0.5F);
		this.setResistance(5.0F);
		this.setCreativeTab(ColoramaReg.cTab);
		this.COLOR = cUtils.Clone(color);
		this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.6F, 0.75F);
		this.setDefaultState(this.blockState.getBaseState().withProperty(VOLUME, 4));
	}

	public BlockPaintcan(Material material, String name, int[] color) {
		super(material);
		this.setUnlocalizedName(name);
		this.setStepSound(soundTypeMetal);
		this.setHardness(1.0F);
		this.setCreativeTab(ColoramaReg.cTab);
		this.COLOR = cUtils.Clone(color);
		this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.6F, 0.75F);
		this.setDefaultState(this.blockState.getBaseState().withProperty(VOLUME, 4));
	}

	@Override
	public boolean isFullBlock() {
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer() {
		return EnumWorldBlockLayer.SOLID;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state) {
		return new AxisAlignedBB((double) pos.getX() + this.minX, (double) pos.getY() + this.minY, (double) pos.getZ() + this.minZ, (double) pos.getX() + this.maxX, (double) pos.getY() + this.maxY,
				(double) pos.getZ() + this.maxZ);
	}

	@SideOnly(Side.CLIENT)
	public int colorMultiplier(IBlockAccess worldIn, BlockPos pos, int renderPass) {
		return cUtils.convertToInt(COLOR);
	}

	public int getPaintColor() {
		return cUtils.convertToInt(COLOR);
	}

	public int[] getPaintRGB() {
		return COLOR;
	}

	@Override
	public int damageDropped(IBlockState state) {
		return state.getValue(VOLUME).intValue();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item itemIn, CreativeTabs tab, List list) {
		list.add(new ItemStack(itemIn, 1, VOLUME.getAllowedValues().size() - 1));
	}

	@Override
	public IBlockState getStateFromMeta(int meta) {
		return this.getDefaultState().withProperty(VOLUME, Integer.valueOf(meta));
	}

	public int getMetaFromState(IBlockState state) {
		return state.getValue(VOLUME).intValue();
	}

	@Override
	protected BlockState createBlockState() {
		return new BlockState(this, new IProperty[] { VOLUME });
	}

}
