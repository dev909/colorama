package net.dev909.colorama.data.blocks;

import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.blocks.BlockStainedPlanks.EnumType;
import net.minecraft.block.material.Material;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockStainedBrick extends CBlock {

	public BlockStainedBrick(Material material, String name) {
		super(Material.rock, name);
		this.setHardness(2.0F);
		this.setResistance(5.0F);
		this.setStepSound(soundTypeStone);
        this.setCreativeTab(ColoramaReg.cTab);
	}
	
    @SideOnly(Side.CLIENT)
    public EnumWorldBlockLayer getBlockLayer() {
        return EnumWorldBlockLayer.CUTOUT_MIPPED;
    }

}
