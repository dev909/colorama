package net.dev909.colorama.data;

import net.dev909.colorama.data.blocks.BlockPaintcan;
import net.dev909.colorama.data.blocks.BlockPrimedBrick;
import net.dev909.colorama.data.blocks.BlockPrimedPlanks;
import net.dev909.colorama.data.blocks.BlockPrimer;
import net.dev909.colorama.data.blocks.BlockRemover;
import net.dev909.colorama.data.blocks.BlockStainedBrick;
import net.dev909.colorama.data.blocks.BlockStainedPlanks;
import net.dev909.colorama.data.blocks.BlockStainedStoneBrick;
import net.dev909.colorama.data.blocks.BlockStainer;
import net.dev909.colorama.data.blocks.CBlock;
import net.dev909.colorama.data.items.ItemPaintbook;
import net.dev909.colorama.data.items.ItemPaintbrush;
import net.dev909.colorama.data.items.ItemPaintcanEmpty;
import net.dev909.colorama.data.items.ItemPrimerBrush;
import net.dev909.colorama.data.items.ItemRemoverBrush;
import net.dev909.colorama.data.items.ItemStainerBrush;
import net.dev909.colorama.data.items.itemblocks.IBPaintcan;
import net.dev909.colorama.data.items.itemblocks.IBStainedStoneBricks;
import net.dev909.colorama.data.items.itemblocks.IBPrimer;
import net.dev909.colorama.data.items.itemblocks.IBRemover;
import net.dev909.colorama.data.items.itemblocks.IBStainedPlanks;
import net.dev909.colorama.data.items.itemblocks.IBStainer;
import net.dev909.colorama.data.tileentities.CBlock_TE;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

public class ColoramaReg {
	
	//MISC --------------------------------------------------------------
	public static ColoramaTab cTab;
	private static String NAME_COLORAMA_TAB = "coloramaTab";
	private static String NAME_TE_CBLOCK = "CBlock_TE";
	
	//BLOCKS --------------------------------------------------------------
	public static Block[] paintcans = new Block[16];
	private static String[] paintcanNames = 
			{"paintcan_WHITE",
			"paintcan_ORANGE",
			"paintcan_MAGENTA",
			"paintcan_LIGHTBLUE",
			"paintcan_YELLOW",
			"paintcan_LIME",
			"paintcan_PINK",
			"paintcan_GRAY",
			"paintcan_LIGHTGRAY",
			"paintcan_CYAN",
			"paintcan_PURPLE",
			"paintcan_BLUE",
			"paintcan_BROWN",
			"paintcan_GREEN",
			"paintcan_RED",
			"paintcan_BLACK"};
	
	private static int[][] paintcanColors = {
			{255,255,255},
			{216,127,51},
			{178,76,216},
			{102,153,216},
			{229,229,51},
			{127,204,25},
			{242,127,165},
			{76,76,76},
			{153,153,153},
			{76,127,153},
			{127,63,178},
			{51,76,178},
			{102,76,51},
			{102,127,51},
			{153,51,51},
			{0,0,0}};
	
	public static Block CBlock;
	public static Block blockStainer;
	public static Block blockPrimer;
	public static Block blockRemover;
	public static Block blockStainedPlanks;
	public static Block blockPrimedPlanks;
	public static Block blockStainedStoneBricks;
	public static Block blockStainedBricks;
	public static Block blockPrimedBricks;
	
	private static String NAME_BLOCK = "CBlock";
	private static String NAME_BLOCK_STAINER = "blockStainer";
	private static String NAME_BLOCK_PRIMER = "blockPrimer";
	private static String NAME_BLOCK_REMOVER = "blockRemover";
	private static String NAME_BLOCK_STAINED_PLANKS = "stainedPlanks";
	private static String NAME_BLOCK_PRIMED_PLANKS = "primedPlanks";
	private static String NAME_BLOCK_STAINED_STONE_BRICKS = "stainedStoneBricks";
	private static String NAME_BLOCK_STAINED_BRICKS = "stainedBricks";
	private static String NAME_BLOCK_PRIMED_BRICKS = "primedBricks";
	
	//ITEMS --------------------------------------------------------------
	public static Item paintbook;
	public static Item paintbrush;
	public static Item paintcanEmpty;
	public static Item stainerBrush;
	public static Item primerBrush;
	public static Item removerBrush;
	
	private static String NAME_PAINTBOOK = "paintbook";
	private static String NAME_PAINTBRUSH = "paintbrush";
	private static String NAME_PAINTCAN_EMPTY = "paintcan_Empty";
	private static String NAME_STAINER_BRUSH = "stainerBrush";
	private static String NAME_PRIMER_BRUSH = "primerBrush";
	private static String NAME_REMOVER_BRUSH = "removerBrush";
	
	public static void init() {
		cTab = new ColoramaTab(NAME_COLORAMA_TAB);
		
		for (int i = 0; i < paintcans.length; i++) {
			paintcans[i] = new BlockPaintcan(Material.rock, paintcanNames[i], paintcanColors[i]);
		}
	}
	
	public static void initBlocks() {
		GameRegistry.registerBlock(blockStainer = new BlockStainer(Material.rock, NAME_BLOCK_STAINER), IBStainer.class, NAME_BLOCK_STAINER);
		GameRegistry.registerBlock(blockPrimer = new BlockPrimer(Material.rock, NAME_BLOCK_PRIMER), IBPrimer.class, NAME_BLOCK_PRIMER);
		GameRegistry.registerBlock(blockRemover = new BlockRemover(Material.rock, NAME_BLOCK_REMOVER), IBRemover.class, NAME_BLOCK_REMOVER);		
		//PAINTCANS
		for (int i = 0; i < paintcans.length; i++) {
			GameRegistry.registerBlock(paintcans[i], IBPaintcan.class, paintcanNames[i]);
		}
		
		GameRegistry.registerBlock(CBlock = new CBlock(Material.clay, NAME_BLOCK), NAME_BLOCK);
		GameRegistry.registerBlock(blockStainedPlanks = new BlockStainedPlanks(NAME_BLOCK_STAINED_PLANKS), IBStainedPlanks.class, NAME_BLOCK_STAINED_PLANKS);
		GameRegistry.registerBlock(blockPrimedPlanks = new BlockPrimedPlanks(Material.wood, NAME_BLOCK_PRIMED_PLANKS), NAME_BLOCK_PRIMED_PLANKS);
		GameRegistry.registerBlock(blockStainedStoneBricks = new BlockStainedStoneBrick(Material.rock, NAME_BLOCK_STAINED_STONE_BRICKS), IBStainedStoneBricks.class, NAME_BLOCK_STAINED_STONE_BRICKS);
		GameRegistry.registerBlock(blockStainedBricks = new BlockStainedBrick(Material.rock, NAME_BLOCK_STAINED_BRICKS), NAME_BLOCK_STAINED_BRICKS);
		GameRegistry.registerBlock(blockPrimedBricks = new BlockPrimedBrick(Material.rock, NAME_BLOCK_PRIMED_BRICKS), NAME_BLOCK_PRIMED_BRICKS);
	}
	
	public static void initItems() {
		GameRegistry.registerItem(paintbook = new ItemPaintbook(NAME_PAINTBOOK), NAME_PAINTBOOK);
		GameRegistry.registerItem(paintbrush = new ItemPaintbrush(NAME_PAINTBRUSH), NAME_PAINTBRUSH);
		GameRegistry.registerItem(stainerBrush = new ItemStainerBrush(NAME_STAINER_BRUSH), NAME_STAINER_BRUSH);
		GameRegistry.registerItem(primerBrush = new ItemPrimerBrush(NAME_PRIMER_BRUSH), NAME_PRIMER_BRUSH);
		GameRegistry.registerItem(removerBrush = new ItemRemoverBrush(NAME_REMOVER_BRUSH), NAME_REMOVER_BRUSH);
		GameRegistry.registerItem(paintcanEmpty = new ItemPaintcanEmpty(NAME_PAINTCAN_EMPTY), NAME_PAINTCAN_EMPTY);
	}
	
	public static void initTileEntities() {
        GameRegistry.registerTileEntity(CBlock_TE.class, NAME_TE_CBLOCK);
	}
	
	public static void registerRecipes() {
		//Paintbrush
	    GameRegistry.addShapedRecipe(new ItemStack(paintbrush), new Object[]{
	            " X",
	            "Y ",
	            'X', Blocks.wool,
	            'Y', Items.stick});
	    //Paintcans
	    for (int i = 0; i < paintcans.length; i++) {
		    GameRegistry.addShapedRecipe(new ItemStack(paintcans[i], 1, 4), new Object[]{
		            " X ",
		            "XZX",
		            " Y ",
		            'X', Items.iron_ingot,
		            'Y', new ItemStack(Items.dye, 1, 15-i),
		            'Z', Items.water_bucket});
		    GameRegistry.addShapedRecipe(new ItemStack(paintcans[i], 1, 4), new Object[]{
		            " X ",
		            "XYX",
		            " Z ",
		            'X', Items.iron_ingot,
		            'Y', new ItemStack(Items.dye, 1, 15-i),
		            'Z', Items.water_bucket});
	        GameRegistry.addShapelessRecipe(new ItemStack(paintcans[i], 1, 4), new Object[] {
                    new ItemStack(Items.dye, 1, 15-i),
                    ColoramaReg.paintcanEmpty,
                    Items.water_bucket});
	    }
	    //Empty Paintcan
	    GameRegistry.addShapedRecipe(new ItemStack(ColoramaReg.paintcanEmpty, 1), new Object[]{
	            " X ",
	            "X X",
	            'X', Items.iron_ingot,});
	    //Stainer
	    for (int i = 0; i < 2; i++) {
		    GameRegistry.addShapelessRecipe(new ItemStack(blockStainer, 1, 4), new Object[] {
		    		new ItemStack(Items.coal, 1, i),
		    		ColoramaReg.paintcanEmpty,
		    		Items.water_bucket});
		    GameRegistry.addShapedRecipe(new ItemStack(blockStainer, 1, 4), new Object[]{
		            " X ",
		            "XYX",
		            " Z ",
		            'X', Items.iron_ingot,
		            'Y', new ItemStack(Items.coal, 1, i),
		            'Z', Items.water_bucket});
		    GameRegistry.addShapedRecipe(new ItemStack(blockStainer, 1, 4), new Object[]{
		            " X ",
		            "XZX",
		            " Y ",
		            'X', Items.iron_ingot,
		            'Y', new ItemStack(Items.coal, 1, i),
		            'Z', Items.water_bucket});
	    }
	    //Primer
	    GameRegistry.addShapelessRecipe(new ItemStack(blockPrimer, 1, 4), new Object[] {
	    		new ItemStack(Items.clay_ball, 1),
	    		ColoramaReg.paintcanEmpty,
	    		Items.water_bucket});
	    GameRegistry.addShapedRecipe(new ItemStack(blockPrimer, 1, 4), new Object[]{
	            " X ",
	            "XYX",
	            " Z ",
	            'X', Items.iron_ingot,
	            'Y', new ItemStack(Items.clay_ball, 1),
	            'Z', Items.water_bucket});
	    GameRegistry.addShapedRecipe(new ItemStack(blockPrimer, 1, 4), new Object[]{
	            " X ",
	            "XZX",
	            " Y ",
	            'X', Items.iron_ingot,
	            'Y', new ItemStack(Items.clay_ball, 1),
	            'Z', Items.water_bucket});
	    //Remover
	    GameRegistry.addShapelessRecipe(new ItemStack(blockRemover, 1, 4), new Object[] {
	    		new ItemStack(Items.redstone, 1),
	    		ColoramaReg.paintcanEmpty,
	    		Items.water_bucket});
	    GameRegistry.addShapedRecipe(new ItemStack(blockRemover, 1, 4), new Object[]{
	            " X ",
	            "XYX",
	            " Z ",
	            'X', Items.iron_ingot,
	            'Y', new ItemStack(Items.redstone, 1),
	            'Z', Items.water_bucket});
	    GameRegistry.addShapedRecipe(new ItemStack(blockRemover, 1, 4), new Object[]{
	            " X ",
	            "XZX",
	            " Y ",
	            'X', Items.iron_ingot,
	            'Y', new ItemStack(Items.redstone, 1),
	            'Z', Items.water_bucket});
	}
	
	public static void initOreDict() {
		//Future use possible
	}
}
