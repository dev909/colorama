package net.dev909.colorama.data.items;

import java.util.List;
import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.blocks.CBlock;
import net.dev909.colorama.data.blocks.BlockPaintcan;
import net.dev909.colorama.data.blocks.BlockPrimer;
import net.dev909.colorama.data.blocks.BlockRemover;
import net.dev909.colorama.data.blocks.BlockStainer;
import net.dev909.colorama.data.tileentities.CBlock_TE;
import net.dev909.colorama.data.utils.cColor;
import net.dev909.colorama.data.utils.cUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCauldron;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class ItemPaintbrush extends Item {

	public ItemPaintbrush(String name) {
		this.setMaxDamage(63);
		this.setNoRepair();
		this.setHasSubtypes(false);
		this.setMaxStackSize(1);
		this.setUnlocalizedName(name);
		this.setCreativeTab(ColoramaReg.cTab);
	}

	@Override
	public int getColorFromItemStack(ItemStack stack, int renderPass) {
		if (!stack.hasTagCompound()) {
			return cUtils.convertToInt(255, 255, 255);
		} else {
			if (renderPass == 0) {
				return cUtils.convertToInt(255, 255, 255);
			} else {
				return cUtils.convertToInt(stack.getTagCompound().getIntArray("rgb"));
			}
		}
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List tooltip, boolean advanced) {
		tooltip.add("Contains paint");
		NBTTagCompound nbt = stack.getTagCompound();
		if (nbt != null) {
			int[] color = nbt.getIntArray("rgb");
			tooltip.add("\u00A74R: " + color[0]);
			tooltip.add("\u00A72G: " + color[1]);
			tooltip.add("\u00A71B: " + color[2]);
		}
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
		// Get block
		IBlockState bState = worldIn.getBlockState(pos);
		Block block = bState.getBlock();
		// Check block against target blocks
		if (block instanceof CBlock) {
			TileEntity te = worldIn.getTileEntity(pos);
			if (te instanceof CBlock_TE) {
				if (stack.hasTagCompound() && ((CBlock_TE) te).setColor(stack.getTagCompound())) {
					worldIn.markBlockForUpdate(pos);
					stack.damageItem(1, playerIn);
					return true;
				} else {
					return false;
				}
			}
		} else if (block instanceof BlockStainer) {
			int stateMeta = block.getMetaFromState(bState);
			if (stateMeta > 0 && stateMeta < BlockPaintcan.VOLUME.getAllowedValues().size()) {
				if (!playerIn.capabilities.isCreativeMode) {
					worldIn.setBlockState(pos, worldIn.getBlockState(pos).withProperty(BlockPaintcan.VOLUME, Integer.valueOf(stateMeta - 1)));
				}
				playerIn.replaceItemInInventory(playerIn.inventory.currentItem, new ItemStack(ColoramaReg.stainerBrush, 1));
				playerIn.getCurrentEquippedItem().setItemDamage(stack.getItemDamage());
			}
		} else if (block instanceof BlockPrimer) {
			int stateMeta = block.getMetaFromState(bState);
			if (stateMeta > 0 && stateMeta < BlockPaintcan.VOLUME.getAllowedValues().size()) {
				if (!playerIn.capabilities.isCreativeMode) {
					worldIn.setBlockState(pos, worldIn.getBlockState(pos).withProperty(BlockPaintcan.VOLUME, Integer.valueOf(stateMeta - 1)));
				}
				playerIn.replaceItemInInventory(playerIn.inventory.currentItem, new ItemStack(ColoramaReg.primerBrush, 1));
				playerIn.getCurrentEquippedItem().setItemDamage(stack.getItemDamage());
			}
		} else if (block instanceof BlockRemover) {
			int stateMeta = block.getMetaFromState(bState);
			if (stateMeta > 0 && stateMeta < BlockPaintcan.VOLUME.getAllowedValues().size()) {
				if (!playerIn.capabilities.isCreativeMode) {
					worldIn.setBlockState(pos, worldIn.getBlockState(pos).withProperty(BlockPaintcan.VOLUME, Integer.valueOf(stateMeta - 1)));
				}
				playerIn.replaceItemInInventory(playerIn.inventory.currentItem, new ItemStack(ColoramaReg.removerBrush, 1));
				playerIn.getCurrentEquippedItem().setItemDamage(stack.getItemDamage());
			}
		} else if (block instanceof BlockCauldron) {
			int stateMeta = block.getMetaFromState(bState);
			if (stateMeta != 0) {
				playerIn.replaceItemInInventory(playerIn.inventory.currentItem, new ItemStack(ColoramaReg.paintbrush, 1));
				playerIn.getCurrentEquippedItem().setItemDamage(stack.getItemDamage());
				if (!playerIn.capabilities.isCreativeMode) {
					worldIn.setBlockState(pos, worldIn.getBlockState(pos).withProperty(BlockCauldron.LEVEL, Integer.valueOf(stateMeta - 1)));
				}
			}
		} else if (block instanceof BlockPaintcan) {
			int stateMeta = block.getMetaFromState(bState);
			int[] color = cUtils.Clone(((BlockPaintcan) block).getPaintRGB());
			NBTTagCompound nbt = stack.getTagCompound();
			// Check to see if nbt is null
			// If not, then set the nbt data as the paintcans color
			if (stateMeta > 0 && stateMeta < BlockPaintcan.VOLUME.getAllowedValues().size()) {
				if (nbt != null) {
					cColor COLOR = new cColor(nbt.getIntArray("rgb"), nbt.getIntArray("total"), nbt.getInteger("totalMax"), nbt.getInteger("num"));
					cUtils.mergeColors(COLOR, color);
					nbt.setIntArray("rgb", COLOR.rgb);
					nbt.setIntArray("total", COLOR.total);
					nbt.setInteger("totalMax", COLOR.totalMax);
					nbt.setInteger("num", COLOR.num);
				}
				// If so, then create new nbt, and set as paintcans color
				else {
					nbt = new NBTTagCompound();
					stack.setTagCompound(nbt);
					nbt.setIntArray("rgb", color);
					nbt.setIntArray("total", color);
					nbt.setInteger("totalMax", cUtils.MaximalValue(color));
					nbt.setInteger("num", 1);
				}
				if (!playerIn.capabilities.isCreativeMode) {
					worldIn.setBlockState(pos, worldIn.getBlockState(pos).withProperty(BlockPaintcan.VOLUME, Integer.valueOf(stateMeta - 1)));
				}
				return true;
			}
		}
		return false;
	}
}
