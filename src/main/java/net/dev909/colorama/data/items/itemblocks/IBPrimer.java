package net.dev909.colorama.data.items.itemblocks;

import net.dev909.colorama.data.blocks.BlockPaintcan;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class IBPrimer extends ItemBlock {

	public IBPrimer(Block block) {
		super(block);
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}
	
	@Override
	public int getMetadata(int metadata) {
	    return metadata;
	}

	@Override
	public String getUnlocalizedName(ItemStack stack) {
		return stack.getMetadata() == 0 ? super.getUnlocalizedName() + ".empty" : super.getUnlocalizedName();
	}
}
