package net.dev909.colorama.data.items.itemblocks;

import net.dev909.colorama.data.blocks.BlockStainedStoneBrick;
import net.dev909.colorama.data.blocks.BlockStainedPlanks;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class IBStainedStoneBricks extends ItemBlock {

	
	public IBStainedStoneBricks(Block block) {
		super(block);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getMetadata(int metadata) {
	    return metadata;
	}

	@Override
	public String getUnlocalizedName(ItemStack stack) {
	    BlockStainedStoneBrick.EnumType enumType = BlockStainedStoneBrick.EnumType.byMetadata(stack.getMetadata());
	    return super.getUnlocalizedName() + "." + enumType.toString();
	}

}
