package net.dev909.colorama.data.items;

import java.util.List;

import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.blocks.BlockPaintcan;
import net.dev909.colorama.data.blocks.BlockPrimedBrick;
import net.dev909.colorama.data.blocks.BlockPrimedPlanks;
import net.dev909.colorama.data.blocks.BlockRemover;
import net.dev909.colorama.data.blocks.BlockStainedBrick;
import net.dev909.colorama.data.blocks.BlockStainedPlanks;
import net.dev909.colorama.data.blocks.BlockStainedStoneBrick;
import net.dev909.colorama.data.blocks.BlockStainer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCauldron;
import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockStoneBrick;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class ItemRemoverBrush extends Item {
	
	public ItemRemoverBrush(String name) {
		this.setUnlocalizedName(name);
		this.setMaxDamage(63);
		this.setMaxStackSize(0);
		this.setCreativeTab(ColoramaReg.cTab);
	}
	

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List tooltip, boolean advanced) {
		tooltip.add("Contains remover");
	}
	
	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
		IBlockState bState = worldIn.getBlockState(pos);
		Block block = bState.getBlock();
		if (block instanceof BlockRemover) {
    		int stateMeta = block.getMetaFromState(bState);
    		if (stateMeta > 0 && stateMeta < BlockPaintcan.VOLUME.getAllowedValues().size()) {
	    		worldIn.setBlockState(pos, bState.withProperty(BlockPaintcan.VOLUME, Integer.valueOf(stateMeta-1)));
				stack.damageItem(1, playerIn);
    		}
		}
        else if (block instanceof BlockCauldron) {
        	int stateMeta = block.getMetaFromState(bState);
        	if (stateMeta != 0) {
        		playerIn.replaceItemInInventory(playerIn.inventory.currentItem, new ItemStack(ColoramaReg.paintbrush, 1));
        		playerIn.getCurrentEquippedItem().setItemDamage(stack.getItemDamage());
        		worldIn.setBlockState(pos, block.getDefaultState().withProperty(BlockCauldron.LEVEL, stateMeta - 1));
        	}
        }
        else if (block instanceof BlockStainedPlanks || block instanceof BlockPrimedPlanks) {
        	int stateMeta = block.getMetaFromState(bState);
        	stack.damageItem(1, playerIn);
        	worldIn.setBlockState(pos, Blocks.planks.getStateFromMeta(stateMeta));
        }
        else if (block instanceof BlockStainedBrick || block instanceof BlockPrimedBrick) {
        	int stateMeta = block.getMetaFromState(bState);
        	stack.damageItem(1, playerIn);
        	worldIn.setBlockState(pos, Blocks.brick_block.getDefaultState());
        }
        else if (block instanceof BlockStainedStoneBrick) {
        	int stateMeta = block.getMetaFromState(bState);
        	stack.damageItem(1, playerIn);
        	worldIn.setBlockState(pos, Blocks.stonebrick.getStateFromMeta(stateMeta));
        }
		return false;
    }

}
