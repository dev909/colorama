package net.dev909.colorama.proxy;

import net.dev909.colorama.data.ColoramaMod;
import net.dev909.colorama.data.ColoramaReg;
import net.dev909.colorama.data.blocks.BlockPaintcan;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ClientProxy extends CommonProxy {
	
	private static final String NAME_RENDER_PAINTCANS = ColoramaMod.MODID + ":" + "paintcan";
	private static final String NAME_RENDER_STAINER = ColoramaMod.MODID + ":" + "blockStainer";
	private static final String NAME_RENDER_PRIMER = ColoramaMod.MODID + ":" + "blockPrimer";
	private static final String NAME_RENDER_REMOVER = ColoramaMod.MODID + ":" + "blockRemover";
	
	@Override
	public void preInit() {
		//Blocks-------------------------------------------------------------------------------------------------
		Item[] paintcans = new Item[16];
		for (int i = 0; i < paintcans.length; i++) {
			paintcans[i] = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.paintcans[i].getUnlocalizedName().substring(5));
		}
	    Item blockStainer = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockStainer.getUnlocalizedName().substring(5));
	    Item blockPrimer = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockPrimer.getUnlocalizedName().substring(5));
	    Item blockRemover = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockRemover.getUnlocalizedName().substring(5));
	    Item blockStainedPlanks = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockStainedPlanks.getUnlocalizedName().substring(5));
	    Item blockPrimedPlanks = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockPrimedPlanks.getUnlocalizedName().substring(5));
	    Item blockStainedStoneBricks = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockStainedStoneBricks.getUnlocalizedName().substring(5));
	    Item blockStainedBricks = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockStainedBricks.getUnlocalizedName().substring(5));
	    Item blockPrimedBricks = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.blockPrimedBricks.getUnlocalizedName().substring(5));
	    
		//Items--------------------------------------------------------------------------------------------------
	    Item itemPaintbook = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.paintbook.getUnlocalizedName().substring(5));
	    Item itemPaintbrush = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.paintbrush.getUnlocalizedName().substring(5));
	    Item itemPaintcanEmpty = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.paintcanEmpty.getUnlocalizedName().substring(5));
	    Item itemStainer = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.stainerBrush.getUnlocalizedName().substring(5));
	    Item itemPrimer = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.primerBrush.getUnlocalizedName().substring(5));
	    Item itemRemover = GameRegistry.findItem(ColoramaMod.MODID, ColoramaReg.removerBrush.getUnlocalizedName().substring(5));
	    
	    //Render Registration------------------------------------------------------------------------------------
	    for (int i = 0; i < paintcans.length; i++) {
	    	registerPaintcanItemRenders(paintcans[i], NAME_RENDER_PAINTCANS);
	    }
	    
	    registerPaintcanItemRenders(blockStainer, NAME_RENDER_STAINER);
	    registerPaintcanItemRenders(blockPrimer, NAME_RENDER_PRIMER);
	    registerPaintcanItemRenders(blockRemover, NAME_RENDER_REMOVER);
	    
	    registerItemRenders(itemPaintbook);
	    registerItemRenders(itemPaintbrush);
	    registerItemRenders(itemPaintcanEmpty);
	    registerItemRenders(itemStainer);
	    registerItemRenders(itemPrimer);
	    registerItemRenders(itemRemover);
	    registerItemRenders(blockPrimedPlanks);
	    registerItemRenders(blockStainedBricks);
	    registerItemRenders(blockPrimedBricks);
	    
		registerItemRenders(blockStainedPlanks, 6);
	    registerItemRenders(blockStainedStoneBricks, 4);

	}
	
	public void registerPaintcanItemRenders(Item item, String name) {
	    ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation(name + "_Empty", "inventory");
	    ModelLoader.setCustomModelResourceLocation(item, 0, itemModelResourceLocation);	
	    
	    for (int i = 1; i < 5; i++) {
		    itemModelResourceLocation = new ModelResourceLocation(name, "inventory");
		    ModelLoader.setCustomModelResourceLocation(item, i, itemModelResourceLocation);	
	    }
	}
	
	public void registerItemRenders(Item item) {
	    final int DEFAULT_ITEM_SUBTYPE = 0;
	    
	    ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation(getFullName(item), "inventory");
	    ModelLoader.setCustomModelResourceLocation(item, DEFAULT_ITEM_SUBTYPE, itemModelResourceLocation);
	}
	
	public void registerItemRenders(Item item, int meta) {
		for (int i = 0; i < meta; i++) {
		    ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation(getFullName(item) + "_" + String.valueOf(i), "inventory");
		    ModelLoader.setCustomModelResourceLocation(item, i, itemModelResourceLocation);	
		}
	}
	
	private static String getFullName(Item item) {
		return ColoramaMod.MODID + ":" + item.getUnlocalizedName().substring(5);
	}
}
